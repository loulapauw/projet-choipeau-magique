"""
Deuxième partie du mini-projet Choixpeau magique dans lequelle nous
créons un site web permettant de tester se personnalité dans le but
d'être attribué une maison par le choixpeau magique.

Auteurs: LAPAUW Lou, SCHOTT Lazare et MURCIAP Albert
Version: Finale le 08/04/22

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/

"""

from browser import document, html
import Code_python

# Importation des questions sous forme de table:
tab_questions = []
with open('Questions2.0.csv', mode='r', encoding='utf-8') as h:
    lignes = h.readlines()
    ligne_des_cles = lignes[0].strip()
    cles = ligne_des_cles.split(';')
    for ligne_suivante in lignes[1:]:
        ligne_suivante = ligne_suivante.strip()
        valeurs = ligne_suivante.split(';')
        dico_questions = {}
        for i in range(len(cles)):
            dico_questions[cles[i]] = valeurs[i]
        tab_questions.append(dico_questions)

# Suppression des chaine de caractères autour des caractéristiques:
for question in tab_questions:
    for i in range(1, 4):
        question[f'Caractéristiques{i}'] = (int(question[f'Caractéristiques{i}'][0]), 
                                            int(question[f'Caractéristiques{i}'][2]),
                                            int(question[f'Caractéristiques{i}'][4]),
                                            int(question[f'Caractéristiques{i}'][6]))


# DONNEES UTILISEES DANS LES FONCTIONS (qui doivent donc être définies avant):

num_question = 0
liste_rep = []

# Bouton innital:
document <= html.P(html.BUTTON("Démarer le test", id = "bouton_start"))

# Affichage des questions importées du fichier csv:
document <= html.P(html.B(tab_questions[num_question]['Questions'], id = 'question'))
document['question'].style.display ='none'

# Création des boutons avec le contenu des réponses du fichier csv:
for i in range(1, 4):
    document <= (html.P(html.BUTTON(tab_questions[num_question][f'Réponses {i}'], id = f"rep{i}", value = i)))
    document[f'rep{i}'].style.display = 'none'

# Création du compteur de question:
document <= html.P(f"0/14", id = 'compteur')
document['compteur'].style.display = 'none'

# Création des boutons des profiles:
for i in range(0, 5):
    document <= html.BUTTON(f"Profile {i+1}", id = f"Ex{i}", value = i)

# Création du bouton final:
document <= html.P(html.BUTTON("Affichez les resultats", id = 'resultat'))
document['resultat'].style.display = 'none'

# Création d'un menu déroulant pour choisir la valeur de k:

dropdown = html.SELECT(html.OPTION(f'{i}') for i in range(5, 11))
choix_possible = [5, 6, 7, 8, 9, 10]
document <= html.P("Choissisez la valeur de k:  ", id = 'choixk')
document['choixk'] <= dropdown


#FONCTIONS :

def start(event): 
    """
    Permet de faire disparaître la page d'acceuil et afficher le test.
    """
    document['bouton_start'].style.display ='none'
    document['titre'].style.display = 'none'
    document['image_intro'].style.display = 'none'
    document['presentation'].style.display = 'none'
    document['profiles_ex'].style.display = 'none'
    for i in range(0, 5):
        document[f'Ex{i}'].style.display = 'none'
    document['choixk'].style.display = 'none'
    document['question'].style.display = 'inline'
    document['compteur'].style.display = 'inline'

    for i in range(0, 5):
        document[f'Ex{i}'].style.display = 'none'
    for i in range(1, 4):
        document[f'rep{i}'].style.display = 'inline'

document['bouton_start'].bind("click", start)


def changement(ev):
    """
    Permet de changer le contenu des questions et des boutons et d'ajouter
    les caractéristiques à une liste selon la réponse.
    """
    global num_question
    num_question += 1
    print(ev.target.value)
    document['compteur'].textContent = f'{num_question}/14'
    liste_rep.append(tab_questions[num_question - 1][f'Caractéristiques{ev.target.value}'])
    if num_question != 14:
        document['question'].textContent = tab_questions[num_question]['Questions']
        for i in range(1, 4):
            document[f'rep{i}'].textContent = tab_questions[num_question][f'Réponses {i}']
        
    else:
        document['question'].style.display = 'none'
        for i in range(1, 4):
            document[f'rep{i}'].style.display = 'none'
            document['resultat'].style.display ='inline'   
for i in range(1, 4):
    document[f'rep{i}'].bind("click", changement)


def resultat(ev):
    """
    Permet de creer un profile grâce à la liste des caractéristiques obtenue
    précédemment.
    """
    document['resultat'].style.display = 'none'
    document['compteur'].style.display = 'none'
    maison_profile =''
    courage, ambition, intelligence, good = 0, 0, 0, 0
    for element in liste_rep:
        courage += element[0]
        ambition += element[1]
        intelligence += element[2]
        good += element[3]
    profile = {'Courage': courage / 15, 'Ambition': ambition / 15,
               'Intelligence': intelligence / 15, 'Good': good / 15}

    voisins = Code_python.length(profile, Code_python.index_id_characteristics)
    kppv = Code_python.kpp(int(choix_possible[dropdown.selectedIndex]), voisins)
    maison = Code_python.display(Code_python.house(Code_python.index_house, kppv), Code_python.index_id_name)

    document <= html.H3(f'Vous appartenez à la maison: {maison[0]}')
    document <= html.P(maison[1])
    if maison[0] == 'Gryffondor':
        document <= html.IMG(src='Gryffondor.png', id='gryf')
    elif maison[0] == 'Slytherin':
        document <= html.IMG(src='Slytherin.png', id='sly')
    elif maison[0] == 'Ravenclaw':
        document <= html.IMG(src='Ravenclaw.png', id='rav')
    elif maison[0] == 'Hufflepuff':
        document <= html.IMG(src='Hufflepuff.png', id='huf')

    
document['resultat'].bind("click", resultat)


def caracteristiques(ev):
    '''
    Permet de trouver les maisons des profiles du premier projet.
    '''
    document['bouton_start'].style.display ='none'
    document['titre'].style.display = 'none'
    document['image_intro'].style.display = 'none'
    document['presentation'].style.display = 'none'
    document['profiles_ex'].style.display = 'none'
    for i in range(0, 5):
        document[f'Ex{i}'].style.display = 'none'
    document['choixk'].style.display = 'none'
    for i in range(0, 5):
        document[f"Ex{i}"].style.display ='none'

    voisins_ex =  Code_python.length(Code_python.PROFILES[int(ev.target.value)], Code_python.index_id_characteristics)
    kppv_ex = Code_python.kpp(choix_possible[dropdown.selectedIndex],voisins_ex)
    maison_ex = Code_python.display(Code_python.house(Code_python.index_house, kppv_ex ), Code_python.index_id_name)
    characteristics = Code_python.PROFILES[int(ev.target.value)]['Courage'], Code_python.PROFILES[int(ev.target.value)]['Ambition'], Code_python.PROFILES[int(ev.target.value)]['Intelligence'],  Code_python.PROFILES[int(ev.target.value)]['Good']
    document <= html.H3(f"Les caractéristiques du profile {int(ev.target.value) + 1} sont: {characteristics}")
    document <= html.H3(f"Ce personnage appartient à la maison {maison_ex[0]}")
    document <= html.P(maison_ex[1])

for i in range(0,5):
    document[f'Ex{i}'].bind("click", caracteristiques)

