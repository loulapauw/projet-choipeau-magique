# -*- coding: utf-8 -*-
"""
Mini-projet choixpeau magique qui permet detrouver la maison
de personnage donnés mais aussi d'un personnage choisi ainsi que
le choix de la valeur de k '

Auteurs : SCHOTT Lazare LAPAUW Lou MURCIA Albert

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/

VERSION 2.0

11/03/22
"""

from KPPV_SCHOTT_LAPAUW_MURCIAP import *
from tkinter import *


# Définition de la fonction pour le programme sans bonus
def action_program():
    print("\nObjectif :\n")
    k_closer = []
    for perso in PROFILES:
        #  Calculer la distance et trouver leurs voisins:
        k_closer.append(length(perso, index_id_characteristics))
    for profile in k_closer:
        #  Trouver ses K plus proches voisins
        neighboors = kpp(K, profile)
        # Trouver leur maison et la maison de leurs voisins:
        print(display(house(index_house, neighboors), index_id_name))

    print("Fin de l'objectif !")


# Défninition des fonctions pour les bonus :

# Pour choisir les k plus proches voisins
def action_bonus_k():
    print("\nBonus : Saisie d'une valeur de k pour l'objectif :")
    k_choisi = int(input('Saisissez la valeur de k : '))
    k_closer_bonus = []
    for personnage in PROFILES:
        k_closer_bonus.append(length(personnage, index_id_characteristics))
    for profile_bonus in k_closer_bonus:
        neighboors_bonus = kpp(k_choisi, profile_bonus)
        print(display(house(index_house, neighboors_bonus), index_id_name))
        print(" ")
    print("Fin du bonus !")


# Pour créer un profile de personnage
def action_bonus_profils():
    print("\nBonus : Création d'un personnage :\n")

    courage = int(input('Quelle est la valeur de son courage ? '))
    while courage > 10 or courage < 0:
        courage = int(input('Courage incorrecte, ressaisissez en un svp : '))

    ambition = int(input("Quelle est la valeur de son ambition ? "))
    while ambition > 10 or ambition < 0:
        ambition = int(input("Ambition incorrecte, ressaisissez en une svp "))

    intelligence = int(input("Quelle est la valeur de son intelligence ? "))
    while intelligence > 10 or intelligence < 0:
        intelligence = int(input("Intelligence incorrecte, ressaisissez"
                                 "en une svp "))

    good = int(input("Quelle est la valeur de sa goodattitude ? "))
    while good > 10 or good < 0:
        good = int(input("Goodattitude incorrecte, ressaisissez en une svp "))

    perso_cree = {
        'Courage': int(courage), 'Ambition': int(ambition),
        'Intelligence': int(intelligence), 'Good': int(good)}

    k_choisi = int(input('Saisissez une valeur pour k : '))

    kpp_perso_cree = kpp(k_choisi, length(perso_cree, index_id_characteristics))

    print(" ")
    print(display(house(index_house, kpp_perso_cree), index_id_name))

    print("Fin du bonus !")


# Interface graphique
window = Tk()
window.title('Choipeaux magique')
window.geometry("1080x720")
window.config(background='#003a91')
window.maxsize(1080, 720)


box = Frame(window, bg='#003a91')

title = Label(window, text='Mini-Projet : Le choipeaux magique',
              font=("Forte", 35), bg='#003a91', fg='yellow')
title.place(x=175, y=100)

button_principal = Button(box, text="Pour faire les profiles demandés",
                          font=("Forte", 20), bg='#003a91', fg='yellow',
                          bd=1, command=action_program)
button_principal.pack()


button_bonus_profils = Button(box, text="Bonus: pour entrer un profile d'élève",
                              font=("Forte", 20), bg='#003a91', fg='yellow',
                              bd=1, command=action_bonus_profils)
button_bonus_profils.pack()


button_bonus_k = Button(box, text='Bonus : pour choisir la valeur des kppv',
                        font=("Forte", 20), bg='#003a91', fg='yellow',
                        bd=1, command=action_bonus_k)
button_bonus_k.pack()

box.pack(expand=TRUE)

window.mainloop()
