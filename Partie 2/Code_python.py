# -*- coding: utf-8 -*-

import csv
from math import sqrt

# Constantes
K = 5

PROFILES = [
    {'Name': 'perso1', 'Courage': 9, 'Ambition': 2,
     'Intelligence': 8, 'Good': 9},
    {'Name': 'perso2', 'Courage': 6, 'Ambition': 7,
     'Intelligence': 9, 'Good': 7},
    {'Name': 'perso3', 'Courage': 3, 'Ambition': 8,
     'Intelligence': 6, 'Good': 3},
    {'Name': 'perso4', 'Courage': 2, 'Ambition': 3,
     'Intelligence': 7, 'Good': 8},
    {'Name': 'perso5', 'Courage': 3, 'Ambition': 4,
     'Intelligence': 8, 'Good': 8}]

#  Importation des fichiers :

with open('Characters.csv', mode='r', encoding='utf-8') as f:
    dico = csv.DictReader(f, delimiter=';')
    tab_characters = [{key: values.replace('\xa0', ' ') for key, values in element.items()} for element in dico]

with open('Caracteristiques_des_persos.csv', mode='r', encoding='utf-8') as g:
    dico1 = csv.DictReader(g, delimiter=';')
    tab_characteristics = [{key: values.replace('\xa0', ' ') for key, values in element.items()} for element in dico1]


# Définition des fonctions

def length(perso, index_perso):
    """
    Permet de calculer la distance entre les caractéristiques
    des personnages types et sur les élèves références.
    ----------
    Entrée : liste de dictionnaires des caractéristiques du personnage type
        et un dictionnaire des élèves références (trié par leur id).
    Sortie : Une liste de tuple avec l'id du personnage et la distance.
    """
    plus_proche = []
    for characters in index_perso.keys():
        distance = sqrt((perso['Courage'] - index_perso[characters][0])**2 +
                        (perso['Ambition'] - index_perso[characters][1])**2 +
                        (perso['Intelligence'] - index_perso[characters][2])**2
                        + (perso['Good'] - index_perso[characters][3])**2)
        plus_proche.append((characters, distance))
    return plus_proche


def kpp(k, tab):
    """
    Fonction permettant de trouver les cas les plus proches
    de certains personnages.
    ----------
    Entrée: un entier(le nombre de voisins), liste avec les id des personnages
    et la distance euclidienne
    Sortie: une liste des K plus proches voisins (ici 5)

    """
    tab.sort(key=lambda x: x[1])
    closer = tab[:k]
    return closer


def house(index, characters):
    """
    Permet de trouver la maison d'un'
    personnage et de donner les maisons de ses voisins.
    ----------
    Entrée: une liste de dictionnaires (index selon les maisons)
            une liste de tuple correspondant aux voisins d'un personnage
    Sortie: une chaine de caractère (la maison du personnage)
            une liste de tuples avec les maisons des voisins
    """   
    resultat = ''
    gryf, rav, sly, huff = 0, 0, 0, 0
    house_k = []
    for voisin in characters:
        for element in index.items():
            if voisin[0] == element[0]:
                house_k.append(element)
    for neighboor in house_k:
        if neighboor[1] == 'Gryffindor':
            gryf += 1
        elif neighboor[1] == 'Ravenclaw':
            rav += 1
        elif neighboor[1] == 'Slytherin':
            sly += 1
        else:
            huff += 1
    if gryf > rav and gryf > sly and gryf > huff:
        resultat = 'Gryffondor'
    elif rav > gryf and rav > sly and rav > huff:
        resultat = 'Ravenclaw'
    elif sly > rav and sly > gryf and sly > huff:
        resultat = 'Slytherin'
    else:
        resultat = 'Hufflepuff'
    return resultat, house_k


def display(tuples, tab):
    """
    Permet d'afficher les resultats.
    ----------
    Entrée: un tuple avec la maison du personnage et de ses voisins.
            dictionnaire qui est in index selon l'id du personnage'
    Sortie: Une chaine de caractères.
    """

    texte = f'Le personnage appartient à la maison {tuples[0]}.\n'
    numero_perso = 1
    for element in tuples[1]:
        if numero_perso == 1:
            texte +=\
                f"Le premier voisin est {tab[element[0]]} et appartient "
            f"à la maison {element[1]}\n"
            numero_perso += 1
        else:
            texte +=\
                f"Le {numero_perso}eme voisin est {tab[element[0]]} et"
            f"appartient à la maison {element[1]}\n"
            numero_perso += 1
    return texte


#   Fusion des deux tables:

tab_poudlard = []
for characters in tab_characteristics:
    for characters_characteristics in tab_characters:
        if characters['Name'] == characters_characteristics['Name']:
            characters.update(characters_characteristics)
            tab_poudlard.append(characters)


#  Création d'index:

index_id_characteristics = {int(character['Id']):
                            (int(character['Courage']),
                             int(character['Ambition']),
                             int(character['Intelligence']),
                             int(character['Good']))
                            for character in tab_poudlard}

index_house = {int(character['Id']): character['House'] for character in tab_poudlard}

index_id_name = {int(character['Id']): character['Name'] for character in tab_poudlard}

# Execution du programme
k_closer = []
for perso in PROFILES:
    #  Calculer la distance et trouver leurs voisins:
    k_closer.append(length(perso, index_id_characteristics))
for profile in k_closer:
    #  Trouver ses K plus proches voisins
    neighboors = kpp(K, profile)
    # Trouver leur maison et la maison de leurs voisins:
    #print(display(house(index_house, neighboors), index_id_name))