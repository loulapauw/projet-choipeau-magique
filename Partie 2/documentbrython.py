from browser import document, html
import Code_python

# Importation des questions sous forme de table
tab_questions = []
with open('Questions2.0.csv', mode='r', encoding='utf-8') as h:
    lignes = h.readlines()
    ligne_des_cles = lignes[0].strip()
    cles = ligne_des_cles.split(';')
    for ligne_suivante in lignes[1:]:
        ligne_suivante = ligne_suivante.strip()
        valeurs = ligne_suivante.split(';')
        dico_questions = {}
        for i in range(len(cles)):
            dico_questions[cles[i]] = valeurs[i]
        tab_questions.append(dico_questions)

# Page d'accueil
document <= html.H1("Test : A quelle maison appartenez-vous ?", id='titre')
document <= html.H1("",id='ligne')
document <= html.IMG(src="choipeau.jpg", width=640, height=460, id ='image_intro')
document <= html.P("Bienvenue dans notre page web pour jouer à quelle maison appartiens-tu ?\
                    Notre mini-projet consiste à répondre aux questions posées de la manière la plus soncère possible.\
                    A la fin des questions, votre maison Harry Potter s'affichera",  id='presentation') + html.P("Vous pouvez également afficher les maison des profils existant ci-dessous :\
                        Profile 1                  \
                        Profile 2                  \
                        Profile 3                  \
                        Profile 4                  \
                        Profile 5",  id='profiles_ex')


document <= html.P(html.BUTTON("Démarer le test", id="bouton_start"))

# Corps du projet

def start(event): 
    document['bouton_start'].style.display ='none'
    document['titre'].style.display = 'none'
    document['image_intro'].style.display = 'none'
    document['presentation'].style.display = 'none'
    document['profiles_ex'].style.display = 'none'
    document['question'].style.display = 'inline'
    for i in range(1, 4):
        document[f'rep{i}'].style.display = 'inline'

document['bouton_start'].bind("click", start)

num_question = 0
document <= html.P(html.B(tab_questions[num_question]['Questions'], id='question'))
document['question'].style.display ='none'

for i in range(1, 4):
    document <= html.P(html.BUTTON(tab_questions[num_question][f'Réponses {i}'], id=f"rep{i}", value= i - 1))
    document[f'rep{i}'].style.display = 'none'
    

liste_rep = []
def questions(event):
    global num_question
    if num_question != 15:
        document['question'].textContent = tab_questions[num_question]['Questions']
        for i in range(1, 4):
            document[f'rep{i}'].textContent = tab_questions[num_question][f'Réponses {i}']
        num_question += 1

    else:
        document['question'].style.display = 'none'
        for i in range(1, 4):
            document[f'rep{i}'].style.display = 'none'
            document['resultat'].style.display ='inline'


document <= html.P(html.BUTTON("Afficher les résultats", id = 'resultat'))
document['resultat'].style.display = 'none'


def click(ev):
    liste_rep.append(tab_questions[num_question]['Caractéristiques'][int(ev.target.value)])
    print(liste_rep)


def resultat(ev):
    document['resultat'].style.display = 'none'
    courage, ambition, intelligence, good = 0, 0, 0, 0
    for element in liste_rep:
        courage += element[0]
        ambition += element[1]
        intelligence += element[2]
        good += element[3]

    profile = {'Courage': courage/16, 'Ambition': ambition/16, 'Intelligence': intelligence/16, 'Good': good/16}

    voisins = Code_python.lenght(profile, Code_python.index_id_characteristiques)
    kpp = Code_python.kppv(voisins, 5)
    maison = Code_python.house(kpp, Code_python.index_id_house)
    maison_profile = maison[0]
    document <= html.H3(maison_profile)


document['resultat'].bind("click", resultat)
document['rep1'].bind("click", questions)
document['rep1'].bind("click", click)

for i in range(1, 4):
    document[f'rep{i}'].bind("click", questions)
    document[f'rep{i}'].bind("click", click)